package com.mutant.dna.laboratory.analyzer;

import com.mutant.dna.laboratory.dna.DnaComparator;
import com.mutant.dna.laboratory.direction.DirectionValidator;
import com.mutant.dna.laboratory.helix.Helix;
import com.mutant.dna.laboratory.model.DnaSample;


public class DnaAnalyzerRight extends DnaAnalyzer {

    public DnaAnalyzerRight(DnaComparator comparator, DirectionValidator validator) {
        super(comparator, validator);
    }

    protected DnaSample nextPosition(DnaSample dnaSample, int step){
        return dnaSample.setCol(step);
    }

    protected  Boolean hasNext(DnaSample dnaSample, int start, int end){
        return start < end && getValidator().allow(dnaSample) && dnaSample.getMatches() <= Helix.COINCIDENCES;
    }

    protected int jumpSize(Boolean isMutant){
        return (isMutant ? Helix.CONSECUTIVES : 1);
    }
}

package com.mutant.dna.laboratory.helix;

import com.mutant.dna.laboratory.model.DnaSample;
import com.mutant.dna.laboratory.direction.Direction;
import com.mutant.dna.laboratory.provider.DnaProvider;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 *   Agrupa los algotimos de extraccion de caracteres: Abajo,Abajo derecha, Abajo la Izquierda y la Derecha
 */
@Service
public class HelixFactory implements DnaProvider<Helix> {

    private Map<Direction, Helix> provider = new HashMap<>();

    /**
     *
     */
    private Helix right = (DnaSample dnaSample) ->
            dnaSample.getAdn().get(dnaSample.getRow()).substring(dnaSample.getCol(), dnaSample.getCol() + Helix.CONSECUTIVES);

    /**
     *
     */
    private Helix down = (DnaSample dnaSample) -> dnaSample.getAdn().stream()
            .skip(dnaSample.getRow())
            .limit(Helix.CONSECUTIVES)
            .map(helice -> helice.charAt(dnaSample.getCol()) + "")
            .collect(Collectors.joining());

    /**
     *
     */
    private Helix downRight = (DnaSample dnaSample) -> {
        AtomicInteger start = new AtomicInteger(dnaSample.getCol());
        return dnaSample.getAdn().stream()
                .skip(dnaSample.getRow())
                .limit(Helix.CONSECUTIVES)
                .map(helice -> helice.charAt(start.getAndIncrement()) + "")
                .collect(Collectors.joining());
    };

    /**
     *
     */
    private Helix downLeft = (DnaSample dnaSample) -> {
        AtomicInteger start = new AtomicInteger(dnaSample.getCol());
        return dnaSample.getAdn().stream()
                .skip(dnaSample.getRow())
                .limit(Helix.CONSECUTIVES)
                .map(helice -> helice.charAt(start.getAndDecrement()) + "")
                .collect(Collectors.joining());
    };

    public HelixFactory() {

        provider.put(Direction.DOWN,down);
        provider.put(Direction.DOWN_LEFT,downLeft);
        provider.put(Direction.DOWN_RIGTH,downRight);
        provider.put(Direction.RIGHT,right);


    }


    public Helix get(Direction direction){
        return provider.get(direction);
    }
}

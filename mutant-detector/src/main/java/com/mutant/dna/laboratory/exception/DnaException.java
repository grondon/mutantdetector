package com.mutant.dna.laboratory.exception;

public class DnaException extends Exception {

    public DnaException(String message) {
        super(message);
    }
}

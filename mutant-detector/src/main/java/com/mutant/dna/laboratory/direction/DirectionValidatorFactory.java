package com.mutant.dna.laboratory.direction;

import com.mutant.dna.laboratory.model.DnaSample;
import com.mutant.dna.laboratory.helix.Helix;
import com.mutant.dna.laboratory.provider.DnaProvider;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 *  Proveedor de los validadores de algorimos basico: Abajo,Abajo derecha, Abajo la Izquierda y la Derecha
 */
@Service
public class DirectionValidatorFactory implements DnaProvider<DirectionValidator> {

    private Map<Direction, DirectionValidator> provider = new HashMap<>();
    private int deep = Helix.CONSECUTIVES;

    private DirectionValidator right = (DnaSample dnaSample) ->
              dnaSample.getAdn().size() >= dnaSample.getCol() + deep    &&
                    dnaSample.getAdn().size() > dnaSample.getRow(); ;

    private DirectionValidator down = (DnaSample dnaSample) ->
            dnaSample.getAdn().size() > dnaSample.getCol()  &&
                    (dnaSample.getAdn().size() >= dnaSample.getRow() + deep);

    private DirectionValidator downRight = (DnaSample dnaSample) ->
            (dnaSample.getAdn().size() >= dnaSample.getCol() + deep ) &&
                (dnaSample.getAdn().size() >= dnaSample.getRow() + deep);

    private DirectionValidator downLeft = (DnaSample dnaSample) ->
            (  dnaSample.getCol() >= deep -1 ) &&
                (dnaSample.getAdn().size()  - deep >= dnaSample.getRow()  );


    public DirectionValidatorFactory(){

        provider.put(Direction.DOWN,down);
        provider.put(Direction.DOWN_LEFT,downLeft);
        provider.put(Direction.DOWN_RIGTH,downRight);
        provider.put(Direction.RIGHT,right);

    }


    public DirectionValidator get(Direction direction){
        return provider.get(direction);
    }


}


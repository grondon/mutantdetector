package com.mutant.dna.laboratory.analyzer;

import com.mutant.dna.laboratory.dna.DnaComparator;
import com.mutant.dna.laboratory.direction.DirectionValidator;
import com.mutant.dna.laboratory.model.DnaSample;
import com.mutant.dna.laboratory.helix.Helix;


public class DnaAnalyzerDown extends DnaAnalyzer {

    public DnaAnalyzerDown(DnaComparator comparator, DirectionValidator validator) {
        super(comparator,validator);
    }

    protected DnaSample nextPosition(DnaSample dnaSample, int step) {
        return new DnaSample(dnaSample.getAdn(),step, dnaSample.getCol());
    }

    protected  Boolean hasNext(DnaSample dnaSample, int start, int end){
        return start < end && getValidator().allow(dnaSample) && dnaSample.getMatches() <= Helix.COINCIDENCES;
    }

    protected int jumpSize(Boolean isMutant){
        return (isMutant ? Helix.CONSECUTIVES : 1);
    }

}

package com.mutant.dna.laboratory.analyzer;

import com.mutant.dna.laboratory.dna.DnaComparator;
import com.mutant.dna.laboratory.helix.Helix;
import com.mutant.dna.laboratory.model.DnaSample;
import com.mutant.dna.laboratory.direction.DirectionValidator;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 *  Analiza la matriz en busqueda de coincidencia de acuerdo
 *  al algoritmo de busqueda, dejando fuero
 *  el algorimo de salto y condicionales de la busqueda
 */
public abstract class DnaAnalyzer {


    Logger logger = LogManager.getLogger(DnaAnalyzer.class);

    private DirectionValidator validator;
    private DnaComparator comparator;


    public DnaAnalyzer(DnaComparator comparator, DirectionValidator validator) {
        this.comparator = comparator;
        this.validator = validator;
    }

    public int analyze(DnaSample dnaSample, int start, int end)
    {
        return doAnalisis(dnaSample, start, end);
    }

    protected Boolean isMutant(DnaSample dnaSample){
        return comparator.compare(dnaSample);
    }


    protected int doAnalisis(DnaSample dnaSample, int start, int end) {

        if (!hasNext(dnaSample,start,end))
        {
            debugger(dnaSample,start,end,-1,false,false);
            return 0;
        }


        Boolean isMutant = isMutant(dnaSample);
        int step = start + jumpSize(isMutant);

        if(isMutant)
            dnaSample.incMatches();


        debugger(dnaSample,start,end,step,isMutant,true);

        return  ( isMutant ? 1 : 0) +  doAnalisis((nextPosition(dnaSample,step)), step, end);

    }

    public void debugger(DnaSample dnaSample, int start, int end, int step, boolean isMutant, boolean hasNext){
        logger.debug("start:" + start + " end:" + end + " step:" + step + " isMutant:"+ isMutant + " validator:" + validator.allow(dnaSample));
        logger.debug(dnaSample);
        logger.debug(( "dna size:" + dnaSample.getAdn().size() + " consecutive+row:" + ( dnaSample.getRow() + Helix.CONSECUTIVES) +  " consecutive+col:"+  ( dnaSample.getCol() + Helix.CONSECUTIVES)));
    }

    protected abstract DnaSample nextPosition(DnaSample dnaSample, int step);

    protected abstract Boolean hasNext(DnaSample dnaSample, int start, int end);

    protected abstract int jumpSize(Boolean isMutant);

    public DirectionValidator getValidator() {
        return validator;
    }

    public void setValidator(DirectionValidator validator) {
        this.validator = validator;
    }

    public DnaComparator getComparator() {
        return comparator;
    }

    public void setComparator(DnaComparator comparator) {
        this.comparator = comparator;
    }




}


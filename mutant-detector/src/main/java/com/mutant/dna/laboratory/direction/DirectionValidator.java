package com.mutant.dna.laboratory.direction;

import com.mutant.dna.laboratory.model.DnaSample;

/**
 *   Algotimo para indicar si se puede recorrer
 *   en una direccion dada
 */
public interface DirectionValidator {

    Boolean allow(DnaSample dnaSample);

}

package com.mutant.dna.laboratory.provider;

import com.mutant.dna.laboratory.direction.Direction;

public interface DnaProvider<T> {

    public T get(Direction direction);
}

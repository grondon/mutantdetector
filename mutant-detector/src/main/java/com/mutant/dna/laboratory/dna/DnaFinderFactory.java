package com.mutant.dna.laboratory.dna;

import com.mutant.dna.laboratory.helix.Helix;
import com.mutant.dna.laboratory.model.DnaSample;
import com.mutant.dna.laboratory.direction.DirectionValidator;
import com.mutant.dna.laboratory.direction.Direction;
import com.mutant.dna.laboratory.provider.DnaProvider;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 *  Agrupa los algoritmos para comprar cadenas de caracteres: Abajo,Abajo derecha, Abajo la Izquierda y la Derecha
 *
 */
@Service
public class DnaFinderFactory implements DnaProvider<DnaComparator> {

    private Map<Direction, DnaComparator> provider = new HashMap<>();
    private DnaProvider<Helix> heliceExtractor;
    private DnaProvider<DirectionValidator> desitionFactory;


    /**
     *
     */
    private DnaComparator right = (DnaSample dnaSample) ->
            allowDirectionAndGetHelice(Direction.RIGHT, dnaSample);

    /**
     *
     */
    private DnaComparator down = (DnaSample dnaSample) ->
            allowDirectionAndGetHelice(Direction.DOWN, dnaSample);

    /**
     *
     */
    private DnaComparator downRight = (DnaSample dnaSample) ->
            allowDirectionAndGetHelice(Direction.DOWN_RIGTH, dnaSample);

    /**
     *
     */
    private DnaComparator downLeft = (DnaSample dnaSample) ->
            allowDirectionAndGetHelice(Direction.DOWN_LEFT, dnaSample);


    /**
     *
     * @param heliceExtractor Provider para la extraccion de carateres
     * @param desitionFactory Provider para las validar si se puede extraer una secuencia de caracteres
     */
    public DnaFinderFactory(DnaProvider<Helix> heliceExtractor, DnaProvider<DirectionValidator> desitionFactory) {
        this.heliceExtractor = heliceExtractor;
        this.desitionFactory = desitionFactory;

        provider.put(Direction.DOWN, down);
        provider.put(Direction.DOWN_LEFT, downLeft);
        provider.put(Direction.DOWN_RIGTH, downRight);
        provider.put(Direction.RIGHT, right);
    }

    private Boolean allowDirectionAndGetHelice(Direction direction, DnaSample dnaSample) {
        return desitionFactory.get(direction).allow(dnaSample) ?
                DnaComparator.comparator(dnaSample, heliceExtractor.get(direction))
                : false;

    }

    @Override
    public DnaComparator get(Direction direction) {
        return provider.get(direction);
    }
}

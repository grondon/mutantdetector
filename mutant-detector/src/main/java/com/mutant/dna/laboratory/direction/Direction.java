package com.mutant.dna.laboratory.direction;

/**
 *   Orientacion de los argoritmos de recorrido de la matriz de adn
 */
public enum Direction {

    RIGHT, DOWN,
    DOWN_RIGTH, DOWN_LEFT;


}

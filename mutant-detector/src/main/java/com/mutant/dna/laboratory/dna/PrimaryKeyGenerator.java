package com.mutant.dna.laboratory.dna;

import java.util.List;

/**
 *   Dada una secuencia de ADN o Caracteres  genera un Hash o Clave primaria
 */
public interface PrimaryKeyGenerator {

    String generate(List<String> adn);


}

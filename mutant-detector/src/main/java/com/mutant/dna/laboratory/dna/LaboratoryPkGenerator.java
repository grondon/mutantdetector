package com.mutant.dna.laboratory.dna;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *   Implementa generar un hash o clave primaria a partir del algoritmo de md5
 */
@Service
public class LaboratoryPkGenerator implements PrimaryKeyGenerator {

    @Override
    public String generate(List<String> adn) {
        StringBuilder b = new StringBuilder();
        adn.forEach(b::append);
        return DigestUtils.md5Hex(b.toString()).toUpperCase();
    }

}

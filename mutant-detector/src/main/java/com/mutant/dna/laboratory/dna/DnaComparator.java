package com.mutant.dna.laboratory.dna;
import com.mutant.dna.laboratory.model.DnaSample;
import com.mutant.dna.laboratory.helix.Helix;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 *   Compara dos secuencias de caracteres
 */
public interface DnaComparator {

    Logger logger = LogManager.getLogger(DnaComparator.class);


    static String getChromosome(DnaSample dnaSample) {
        switch (dnaSample.getAdn().get(dnaSample.getRow()).charAt(dnaSample.getCol())) {
            case 'A':
                return "AAAA";
            case 'T':
                return "TTTT";
            case 'C':
                return "CCCC";
            case 'G':
                return "GGGG";
        }
        return "";
    }

    static Boolean comparator(DnaSample dnaSample, Helix helix){
        String helixExtractor = helix.extractor(dnaSample);
        String cromosoma = getChromosome(dnaSample);
        Boolean isMutant = helixExtractor.equals(cromosoma);
        debugger(dnaSample,helixExtractor,cromosoma,isMutant);
        return isMutant;
    }

    Boolean compare(DnaSample dnaSample);

    static void debugger(DnaSample dnaSample, String cadenaAdn, String cromosoma, boolean isMutant){
       logger.debug("ADN SIZE:" + dnaSample.getAdn().size() + " ROW:" + dnaSample.getRow() + " COL:" + dnaSample.getCol() + " MUTANT:" + isMutant);
       logger.debug("HELICE: " + cadenaAdn);
       logger.debug("PATRON: " + cromosoma);
    }


}

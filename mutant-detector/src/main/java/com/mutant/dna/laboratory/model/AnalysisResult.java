package com.mutant.dna.laboratory.model;

import com.mutant.dna.laboratory.direction.Direction;

public class AnalysisResult {

    private Direction direction;
    private Boolean adnMutant;

    public AnalysisResult() {
    }

    public AnalysisResult(Direction direction, Boolean adnMutant) {
        this.direction = direction;
        this.adnMutant = adnMutant;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Boolean getAdnMutant() {
        return adnMutant;
    }

    public Boolean isMutant() {
        return adnMutant;
    }

    public void setAdnMutant(Boolean adnMutant) {
        this.adnMutant = adnMutant;
    }

    @Override
    public String toString() {
        return "AnalysisResult: {" +
                "direction= " + direction +
                ", isMutant =" + adnMutant +
                '}';
    }
}

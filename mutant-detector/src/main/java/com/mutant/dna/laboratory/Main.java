package com.mutant.dna.laboratory;

import com.mutant.dna.laboratory.mutant.Laboratory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


@Component
public class Main {

    //  4 - 7
    //   0123457
    static String dna1[] = {"TTGCGATA",   // 0
            "TGGTGCTC",    // 1
            "TTATGTTT",    // 2
            "TAGAAGTG",    // 3
            "TCCCAATA",    // 4
            "TCCCTGTC",    // 5
            "TCACCGTT",    // 6
            "TGATCGTA",    // 7
    };

    static String dna[] = {"TACAGGACTAATCCAAGCCATAATTACGAACGGTGCTCTTAAGTCAGCTTAACTAAGGTCTGAATTACGTAAAGCCGCAAGGACATGGCGGCTCTCGCAC",
            "CGGGGCACTAACCCCTGAGATTGGGATTAGGTCTTCCGCCCAGCATTACAGAGGTTGTCCCAAGCGAGACTATTAAATGGCTCTGTTTCCCCCACCAAAC",
            "ACCGAGAGGGGGGTAATCCAGAACCAGAAAGCGTCTAGTCATGCTCAGCGCTAACAAAGTAGACTGTTGTGGTGTGTAGGCCCTGTCAGCGGTCATAACC",
            "AGACTTAGGAAGTCCTCCTTTAATCGCCTTCAGTAGTACTCACACGGTCGGGGCGCTGCTTACTTCAGAGCGTCCAGGAAACAAAAGACTATTGAGGGCA",
            "CTGTGGCGGGGTTTCAAGGTATAGCTGGCAAGGGCCTGAATTCATCATTTTAGCCCCTTGGTTAACCCTCTCTGCTAAGGTAAACTTACAGGGGAAATTG",
            "GCTCGATTATAGTTTTCCGACCCACCATAACACTCGTGGTCCAGGCATGTCAAGCAGTGATTTTGACGAACCCGTAATGGCTAGGCTACACAGTTTTGAG",
            "TCACAATCACACCCTCCCTGATCGACGACTTCCGTGCTACTTGCTTTTGACAAGAAGAGGGACGAGGATACCATGGTGGCCATTCAAACTTGAGGGCTCC",
            "TCAGGAGTCCTTATGCCTGTAAAGCGTGTCGTAAAACTCTATCCCGTTCAGAATCTTGATTTATTCTCTGCGCCGGGGCCACAGTTCTCCTGGAGCTGTG",
            "ACGAACCCCTCCCACTCGCCGCACCTCCCGCGTGATCGCCAGGTTATCACGAGATCGGAAAACAGAAGAAATTCACTGGAGATTAAGGCCTTTCAACCTG",
            "CGGACGATTGGTTGCTGGGGTCTGCGTACATGGAGCTATCATCATCGATGCTGATAATCGTGAGCAACCAAACGCGCAAGGATGGGTACTTGAATCGCTG",
            "TGCCCCGGGAAAGCATTTAGCACCGCTGATACGAAGCGCTAGGTCGCAACCGACTCGCGCGATTGTATACACAAGGATTATGGAGGCTTCCTAGCGTAAA",
            "TAGGAAATGCATCGCGGATCACACGTTCTCGCCGTTGCGTGTCTGCTGGTATGGTATGACCTGCATTCCCTACGCTCTCCAACCTTCTGTTAAACGGTTT",
            "CACGGATGTTCAAGCGTCTCCATCTACTCTCAGACCTTTGAGAGATGACCCAGTCTGCCCGGATACGTCATCGATGATGTGGAAGAATGGAGAGTGATTG",
            "ACTAACGACCCAGTAAAAACGTTGGCCTCAAAATCCGGACGAGGTCCCAAGACCTTACGGCACAATTCTCAGTCTTTTACCCTACTCATGGGGCACAAGA",
            "TCCACTCTCTCCTTTGGTATGTGCCCGTCGTTGGGACGGCCAGTTCAAAATGTGGTCGCGTATTACTACGCTCCGGTGTTTCATTCCATCCAGATGTTTT",
            "GTGCCCTTTAAGGGCTACCCTGCATCTGGGACCTTCCCTGAGTTCGTTGACGCTCCAGACAAACCATCCCGGGTCAAGGACGCACAACCTCAGTTTCAGT",
            "CGGAATGTATGGATCGAGCTCCCACTTAATTAAACTTATTCCAACACTCCATATGGTGAAACGATTACTTATGTAACGGGCGTGTGCGCTAACTCTTGGA",
            "ACAATTTAATGGGCATCAATGGCACAAACTACTGTAACGTGGATGCGCGTTCCCCGAAAGTCGCCTAGGACCCTTCCTATCGCTTTCGAAAAGGTGCTTG",
            "TCGCACCTTCGGCGTTCCGAAAGCTCAGATAGGGGCTAGTTCAATTACATGCGTAACAGAAGCTCGCCTCGGAGACTCTCGACCGAGAGTTCCGCTAGAG",
            "GGAAATCTTTCTAAACCAGGATTAGTGCACCTTACCAGGCGCTCGCAAAATCCTATGCCTTTGTGCGGGGGCTCTGGGTTGCCCAAAGGTGGAGCCTCGA",
            "CATCCCGCTTTCGTCTAACCTGCGAGTTCCATTCCCGCCGCGTTCCACGGGGGGTTGTGTCGTCAAACCCTCGTAAACCCCAGGACACGCATTCCACATA",
            "ATAATCGATTGATTTTTACGTGTTGGCGGCTATGCTCCCTCAACAAGCAGGCCCATAGTCCGTGGGTTAGAGTTCTGGATTGGGGGTAAGGTGTCTAGTC",
            "ATACGATCATAATTGACAACCCTAAAAAAGTGCGGTGGGAAGTGCGCAAGGTGGTTAGGCCTACCGTAGCGATGAACAGTGTAGTAACCACGAAACTCTA",
            "GGGTATTGGAATCGCGGGCTTTAAACGGCGCCGAATTGCACTCATCGTGCCCGATTCTCGTGGTACGGTACAGCTCAGCTTCATTTGTCAGAATCAATGA",
            "ACCAGACTGCTTATGCAATCCTAACGGTGATGTACTCAGTATCAGCGAGTGACGTTCGCTGCAATGGAGGGACAGATAGATGCGCATCCGTTGAAACCGG",
            "CAATTAGGCGGTGTGCCGTTGATGGTGGGTATGTTTTACCGCTGGTAGACAGTTTACGTGCGCTCCGGTTAATTCTGGGCTTTCTGGAGTTATCAACTTA",
            "GGCGTCATGCTGTGCCTTTAATCATGTCGCACGGAATTGGCACCATTTTGATATCGCCCCAAAATAAGCAGATTCATAAACACACGACCTATTTTGGGGT",
            "CTTCTTTACGTACGGCCGTAGCAATGCTCATTGTATCTGCGCGCACTCCCTTACTGCAAGGCGTACCATTCTCTTTATATAATAAGCGATGATTATAAAA",
            "GGTCATATTCCCGTGCCGCACTACCGGCCACCGCTGCGAGGATTGGATAAACTCACGCAAAATGATCACCTTGGTTGTGGATTTTAAAATTTATGGTTTT",
            "GTGGTTCCGCATGTTAGGGGGAGTGAGTAGTTATTACTCGGGCAGAATGAACCCCTGTCGTATCCAGATTCCCTTTCAAACAATGGCCATGACTTCGTAC",
            "GGGTCGCGCACTTCACCGACACTAGCGTATCTTTAGAAGAACGACTGGCAACAGTACTGTTCGACCGTGCGCAGCTTCGTAAAATGAGGCCGTATGCCAT",
            "TGCACTAAGATGCACGCACGAACAGGAAATTTTGACGTACAAGCACCAGTTTATTGCTCAAAACGGCTCACTCATTTTGTCTTACTAGCCTAACGTTTAA",
            "CATGCCTGGGTGCTAGAAGTAAGCCTAGATGAGTGCTCGTACGCAAAGCACGGCTTGGCTCAATCATACTACTGTGTGGTAAGCGCGAGTTTAAAACGTG",
            "ATTGGCTTATGCAGGCAGGGCTGGAGGCCGTTTTGCTCTGCCGTCTGTGGCTTCCCGCATAGTACCCCTACGGATATCTTAATACTTTGACTATTAATGG",
            "AACGGACTCTTTTTGGCTCTGGCCGAGGCAGGGACTGCTGCATATCGCCCAAGCCCAATTCGGGCCTACGGCAGTATCCCATCTTTTGATGATGTCGTAG",
            "TATTGACTGTAAACCCCGATTTGTGTAGAATCAGCCATTTAACGCTGGATACGGACCGGTTCGGGTAAACACGGCAATTATGCAGCGTGTTGGCCGTCCA",
            "AGAGTCGTCTTCGGAGCTCCAGCGTGATCACCAGAAACCGCCAAGGCGTGGAGCTAATGATGGCCACCTGACGCAAAGCGAGCGGAAGTCGGTTGAGGAG",
            "CAGGATGGTCGACAGGACAGTCATCGACCGGGAATTACGTGTATGTGGACAGATTATCTCGGCCCATCCACCCCTAGCCATCGCACGCCAGCACATGGCG",
            "TTCCTGCGGTCGCTGTCTCTTATCTCCGGCCCATCTCCGTAGCCTTCCCGGGGAGCACTACATCGCTACAGTGTTAAATGGGAGCGACGTGAACCCGCGT",
            "CTCTCCCCGTCCACAAGCGAGTTACTGGCTGATATCTGTTTGCATATCGTAGGTCGCGCGCCTCGGCCGGTAAAGGGACTACGCAACGTCCGCACGTCTT",
            "CCCTGGTAGATAGCGACAAACAAGTTCTGGTTGCGGCACAGAACGAGAACTGATTTGGCACGGTATCCACCCTCTTCGAGTTGTGCTCCTGTTGGTTCAA",
            "TCCTCAAACCATCAGCTTTGCTTTCCACTTAGACACATCCCAAGTGAAGGTGTTGCTCTGCCGCATCTACCCGGTTTCATAGGGATTTAACACTGGGGCT",
            "GTCTATAGGAGACTCGTTAAACCTGGGGAGTGCATTTGCTAATCCAGGGAATGACTAAACTAAAGCATTGTATGGCTGTACCGATCGTTACCGAATGCTA",
            "GCCTATATCCACCGGGCGGTCAGGTTCATGCCAGTTCCAGGAGCACTGTGAAAAGAAAGTAGTTTGTAGAAATGATTGGTATGATATTACATCCCTGGGA",
            "TGGGGCCCTCGCAGGCCGCGTGTTCAATGTGTACACATACTAGGGTCACTCCTTCACATCAGCACTCCAAGAATACATATTGTCGAATCGTTGATCGTTG",
            "CCTTGTGAAATGAACTCGCGTGAAGCCTAATGGAAATGGCTTTAAGTATATGGCATTTCATACATTGGATGGCTCGAGCTCGAGGATAAAGAACTATTAA",
            "AGTGTGTTTGACTGGTCCGGACAATCCGATGCCCTTGGAAATGTCACCCAACTTTTCCAAACGGTGTAGTCCGAATCAGAGGTCGATCGAGGGGCGAGTA",
            "TAACTACCTGCATAGTGCGATTCTTCTATAGGCGCGCATTCGTAGGGGATTATTTGCTCACCTTACTCCTAGCATATTATGGAAGAGATAAAGGGTGATT",
            "TACTAGTTTCATTCTGAAAGGCGCATCTGCTTATTGATGGCCACGGCCTCCGCGATCTCTAGCCCCGCCCCAGCCGAGTGACATTATCGCTTCAACGGTT",
            "GGGGAATCCTACCTCCAAGTAGCGGTGAGTCTAGGCATAATGGGCGTGCAGCCGATCCTCGCCCCAGCAACAAGCCCGCGTTTACCACGTCAATTCAGCG",
            "GGACACGGATAGCCGTGAGTAGTGTACAGATTTGACTGTATTTAATTGACGAGACCGCTTTCCCGAAACAAATTCAATGGAGTCCTCGTCTCGTACGGCA",
            "CCCTCCTGCCTTACGCGAGCTGCAGTTTTTGTCTGCGCGGGGATTAAGATTACTTTCTGTCCCGGGCTATATTATAGCTTCAAGAACACGACCGCATACA",
            "TCATGGGAAGGTTGTGGAGGATAATTGATGAGTCCGTTCACGATTAGACAGAAACACTGGGAAGACGTACGCGTGTTTCCATTAGTTCAGTCAATAAGAC",
            "GTCTTGACTTTTACGGAGTGCATCGTAGTTGTGTAAGCCTCATGGTAGGCTACAACCAGTCGCTAAACCCGGTATCGAACCCACACACTGATCGTAATTT",
            "TACAGAGAAAAGATTGAGTACGAGACGCGTCAGATCCAATTCGAAGTCCGGCCGGCGTGGGATCCTGACAATACATAGCCACTGAGCCCAAACCAAGGTT",
            "GACAGGGTGTGCCGGCCCGGTTCATGTAAGACGGGTTTTTACCGATGCGCACATGTATTACAAGCGTAATCATCTGTGTCAGGAAGCCACTCGAGCCGAG",
            "GCGGTTGGGTCGCTTAACTAAAACGCCCGGGGAATAGAGTTAACGGAAGAGTCCTACACGGCTGCATATAGATGTTATAACAATAGACACCTTTCAGATC",
            "GTGCGAACTCAGCTGTGTCTTTAGCAACTTGTATACGATACCGAAGGAAACTATTAAGATGCATAACCGAGGAATGAGTATTCGTACGTCAAGTCGACGG",
            "CAATGGACTTCTGCTCGTTGGCCTGTGCCCCGGAGAGGCCTCAGCTTTCATGCTGGATGGAAGCACAGCCATGAAAACCTCAGTTGATTACTGACTACTA",
            "ACGTGCCTTATTTTGGAATCAAATAGTGAAGTCATGGTGTCTGCTTGCCTCTACGACCCCCGTCATTCACCTGGAGTCGGGAAACAGGTTGGTTCCAAGA",
            "TTGGTGGCACCAGCACGATGATATTTATGTATGGAGCAAACCAGAGACCGCATCTGTACGGTTACTTCGGTGCGTTTGTCCTGGAAGAGCATCGCCAGAC",
            "ATAGAGATCACGGACAACTCTAATCTGCTTGGACGACCTTATGCCTTATCATGCCAAGGCAAATAAGCACTGTGGACACCGAAGATATAGAGTGTACCTG",
            "TCTCCGCATGCTTGCCTAGTCTGCTTCACCCAGGCGGGACACACTCATTATGGGTACTCGTGCAATTGATGCAATAGTGCCTCCTATAAACTCACTCATC",
            "CAGATGTTAGTTCAGGGGTTTGTGAAGTCAGAATGTATCTAGTTCGGTGAGATCAACTATCCCAAGTGCATTTTAGGATTGGTTGGTACTGAACCAGTGG",
            "TGGCCACCTGCGGATAGATTTCTATGGATACACGCGAGCAGCATATTCCTTTAACGATACAGGCGTAACATCTTTAGTAATTCTAGAGGTCGTGACATAT",
            "GCCGAGGGAATAAGCGTGAGTATTGGGCTCGTCCGGTACATACTCACTGTCCATTCAAGCTGACGTGCATCACTAGCTGTTAGCATCCAATGGAAACTCG",
            "TAGTATAGCCGGTTAAGTAGTCAATCAGACTCTCTAGTGGGAGGCGGTCTCCTCAGCAACGTGTATATATATAGAATTACCAGTTTAGTCGATGCCGGCC",
            "AAGCGCCGGGGAAGTATCAGCGCGATGGACGACCAGGTCATACGATTGATAGCGGCCTTCGACAATACCTGGTAAACTCAGCATAGATTCACTATACTTC",
            "CCGCTAGTTATCGTGCTGATACATTCCCCCTTAACTAGGGTTCACCGTTCTGGGCGAAAGTAGCAACGGATATCGCATTACTGGAAGAGTTGCACGCGGC",
            "GTGTTTGTCGCACCCCTAGTGCAAAACACGGTTCATACCTTTCGGACCCGGGAGGCGATATCGCATATCATGGCCGATTATCATTGACGGGAATTTTTGC",
            "AAACTGCCCGGAGAGTGGAGGCCATTGTTAGCTCCTTCATTTACACGCTAGACAAGTAGTAAGGGAGTTTATTAGATAAATGCATCCGGATCCAGACACA",
            "CGCTGGCTCTGTCCGAATGGTGCCAAACTAGAGAAGTGTTCTCCGTAGAAATCAGCGTATGAAGGTGCCACATGGGCATATGAAAAGATCTGGCCGCTGG",
            "AATGGAGTTGAAATTTTCTGGCAATGCTTTGATAAAGCTTGCATTCATGAGGAAAGAGAAGGCCCAAGCAGGTTGCGGCTCACACAGTTTCAATAAACTC",
            "TTCTCACTACATCTTATATGGGTATTGGTCGGCAATACTAAGCGTAACTTGAACTCAACATCACGACGTCATTCATGTGACGCTCCATTAGTAAATCGCT",
            "ATGACGCTTCTATCGGCGTATAAGGATCAGCAGTGTCCAGGCGCCCGTGCTAGGTGCTGAGTATTTAGTGCTCCACGGGAGAATAAATGCGCAGAGGCGT",
            "GCCGTTGTGATCTTGACGATATGCATGAGCCAGAGCGAAAATTTGGGTGTCTAATCGTGTAGCATTTCAGAATGAACCTTTAGCTGGAGACACACATGCC",
            "GTGCAGTCGTGAATTTGAATCCCTTCGACTTTACACTATCTCTCAAGACTCAGTCTCGGTTCAAGGATTCGCCGTAACATCAATATCTCATAAGCTGGTG",
            "AATGGTACGGTGTCAATGAACTTTTGACGACGCCGGCTTCCTCTGATATATCGCTCGCGGGTTGCGGCGGGCACCACGCTTAACCCTACGACAAACTTAC",
            "GAGTTTAGGACCCCTACGCTTTCTGGGAACACTGGGAAGCTTAATTACAATGAGGTGGGAAACGCTCCCGTACCAGTCTGGTGTATCCAACGGTCAATAA",
            "GAATTCGTGAAGTTTATCTTCATGCGAAAGGTAATAATCGAAAGTCTGGGATGAAAGACTCGCGAGAGACGACCGATCGCCCGCCCGAGGATGCCGCCTG",
            "ATCTAGCACTCCACTTCTTGTTGGGGTCTCTTTATGCTAAGCCAACAATTATTCGTCTAGGAGTGGAACACAAGTACAGTTACCCTCCGAGGCCTCTGCC",
            "TATAGAGGTGACTGACAGCCGTGTGTTATCTCCTACCAGGTCGATCGTACGAAAAGATTAAGTCATGGAAGCCCTCCGAGGGGTTGTTCGGTTCTTCACC",
            "CAGTGATTGATACTTAAACTGATCATACATTTCGTCACAGCAAAAGACGGATACAGTGTGCGTTCAAACTACAGAACAGTAGGTAAGGGCCGGGGTGGCA",
            "ACTGTAGGCATGTTGCCTGAGTTGTGAGCCCACTAAAATCGTCCCAATTTATGAACAGATGGAGGACGCACCGCTGTTCGAACTCGTGGTGTGTAACGTC",
            "TATCTACCTCTATGGCTGACCCTAATCGTACTTCCAGACCACCCGAGTACCTATCGTTTAAAAGTCTGCATGTGTCTGTCGTCACAACCAAGAGATTATC",
            "AGTTTGATCTGGAATTCACCCTAACATGATTAATGGATACAGTGGTTGTTGAGATGTCGAGCTCCCTCAGTACCTTACGCCACCTGGCTTCAAGAGACGT",
            "TCGGATGCCCAGTTAGGGGTTCGGAGACAATATCATATCTTTCTTCCATATCCCCGCTATGGACTGTGACATTCGTACTGTCCGCGTTCGACATCCGGTA",
            "TTTCTCAAAACAAAACACTAAATGCCCGCATCCCACTCCCAGGCAGGTGTCGGGTCACCTGTGCGACTTCATCATCGACATGTTCTTCACCGAAGTGAAG",
            "CTACGATGCTCGGCTTTGGTACCCACATGGGAGATTTTATCTTGGTTTCGATCAAAGTCAAATGGAATGCTGAACAGACTGTGGGAATCATAACAGATAA",
            "AGTTAATGGGGCAAATGACGAAAAGGCCTCGATCAACTGGGGCGCGATTGCGGCCTCGGAGCTCAAGTATAATTCTCCCTCTCTCCCAACAGCTAGCCAA",
            "CGGAGCATAGGTCAGTGTGTTAAAAGTGCCTGCCAGGCGTGTACCATTAACCTCGCAACCGAGTTCCGTGAGTATTAGAAAGAACTAAGTCAAGTGATAT",
            "TGTCGCGCCATGTAACCGTGCCATGTAGAAAAATGCACTAATTCTGGAAGCTCAGAAAAGCCCTCTTATTACCTGGCGTACTCACATGTGGTCTTCACCG",
            "GTGAGTCTAAACCTCGGCTTTCACAATTGTTCTTCTTTTGATTTGCGAGCTTGCTGATACCGTTCCGCGCCTAAGTCGCACATACTGGTAGTCTGTCATT",
            "ATCGGAGCAGTTGTTCGTCTAAGGGGATACTTGTTTTAGTGGTAACATTTAGAATTTTCGGTATCTACCAAACCCAGGAGAGGCCGTCTCCATACGTCAT",
            "TATAGATGGACCAGTATCGTAGCTGTAGGTATTCCGCTTCCTGATAAATGAGGATCGCAGGAGAATCCCACTAGCAATAGACACTAAGCCCTATGCAAGG",
            "CAGTTGTTAGCTTTCTCAAATCCACCCTTTACACGGTCCCAATAGATCATACGCTGACTCTATAGAACTGAGTAGAATCAGTCGACCCGTTGACTTGTTG",
            "ATTACCGGACCGTCGCTAGAGCACTCTGCAGAACGGCCAGGCCTCACTCAAGAAGAGCCCTAGTTAAATCCGAAAGTCCTGTGGAACTGTTAGTTTTGCC",
            "CTGAAAGCGCACTCTTGCGTATCGTATTACGTGGGACGATTTCAGGAAAGGTTGCAGTCCGCACACAGGGGTACAACCGCTTGTATCCCAATTCGTCCAT",
            "CTATAAGGTGTAAGTAGGATGGGTGAGGAAGTCCGCCCAGGCTTTGTAAATTAGTTCCCCCCATTAAGATTGTCACGATCCCCACCCACTTCCCTAACGA",
            "CGTTCTGCTGACGGATATACCATGGGCGACAGAAGAACGTACCTCCCGCGATAGAAGAAACTGCGAGAGCGCGTGGAACTGTTTACAAAGACTGTACTCA"};

    public static void main(String[] args) {
    }


}

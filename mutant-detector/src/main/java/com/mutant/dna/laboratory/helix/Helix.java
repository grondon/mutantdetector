package com.mutant.dna.laboratory.helix;

import com.mutant.dna.laboratory.model.DnaSample;


/**
 *   Extrae una secuencia de caracteres de una matriz
 */
public interface Helix {

    String extractor(DnaSample dnaSample);

    static final int CONSECUTIVES = 4;
    static final int COINCIDENCES = 2;
    static final boolean debugger = true;

}

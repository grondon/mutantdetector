package com.mutant.dna.laboratory.mutant;

import com.mutant.dna.laboratory.exception.DnaException;

import java.util.List;

public interface Laboratory {

    boolean analysedAdn(List<String> adn) throws DnaException;
}

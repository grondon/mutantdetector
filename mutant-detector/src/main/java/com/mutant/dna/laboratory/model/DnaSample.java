package com.mutant.dna.laboratory.model;

import java.util.List;

public class DnaSample {

    private List<String> adn;
    private int row;
    private int col;
    private int matches;


    public DnaSample(List<String> adn, int row, int col) {
        this.adn = adn;
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public DnaSample setRow(int row) {
        this.row = row;
        return this;

    }

    public int getCol() {
        return col;
    }

    public DnaSample setCol(int col) {
        this.col = col;
        return this;
    }

    public List<String> getAdn() {
        return adn;
    }

    public DnaSample setAdn(List<String> adn) {
        this.adn = adn;
        return this;
    }

    public int getMatches() {
        return matches;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    public int incMatches() {
        return ++this.matches;
    }

    @Override
    public String toString() {
        return "DnaSample{" +
                "row=" + row +
                ", col=" + col +
                ", matches=" + matches +
                '}';
    }
}

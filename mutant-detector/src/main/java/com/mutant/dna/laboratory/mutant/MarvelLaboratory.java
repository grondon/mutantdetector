package com.mutant.dna.laboratory.mutant;

import com.mutant.dna.laboratory.analyzer.DnaAnalyzer;
import com.mutant.dna.laboratory.analyzer.DnaAnalyzerFactory;
import com.mutant.dna.laboratory.direction.Direction;
import com.mutant.dna.laboratory.exception.DnaException;
import com.mutant.dna.laboratory.helix.Helix;
import com.mutant.dna.laboratory.model.DnaSample;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarvelLaboratory implements Laboratory {

    DnaAnalyzerFactory factory;

    public MarvelLaboratory(DnaAnalyzerFactory factory) {
        this.factory = factory;
    }

    /**
     * Analiza la matriz en busqueda de coincidencias
     * @param adn
     * @return
     * @throws DnaException
     */
    public boolean analysedAdn(List<String> adn) throws DnaException {
        try {
            int match = 0;
            for (Direction direction : Direction.values()) {
                match += process(direction, adn);
                if (match >= Helix.COINCIDENCES)
                    return true;
            }
            return false;
        } catch (Exception ex) {
           throw new DnaException(ex.getMessage());
        }
    }

    /**
     *
     * @param direction Recorre la matriz de acuerdo el algoritmo de busqueda
     * @param adn
     * @return
     */
    private int process(Direction direction, List<String> adn) {

        switch (direction) {
            case RIGHT:
                return rihtSearch(direction, adn);
            case DOWN:
                return downSearch(direction, adn);
            case DOWN_RIGTH:
                return downRightSearch(direction, adn);
            case DOWN_LEFT:
                return leftSearch(direction, adn);
        }

        return 0;

    }

    /**
     *
     *  Recorrre la matriz de abajo hacia la derecha
     * @param direction  Posicion incial del recorrdo de la matriz
     * @param dna Matriz de adn
     * @return
     */
    private int downRightSearch(Direction direction, List<String> dna) {
        DnaAnalyzer analizer = factory.get(direction);
        int totalCase = 0;
        for (int i = 0; i < dna.size(); i++) {
            totalCase += analizer.analyze(new DnaSample(dna, i, 0), 0, dna.size());
        }
        return totalCase;
    }

    /**
     * Recorre la matriz hacia la decrecha
     * @param direction Posicion incial del recorrdo de la matriz
     * @param dna matriz de adn
     * @return
     */
    private int rihtSearch(Direction direction, List<String> dna) {
        DnaAnalyzer analizer = factory.get(direction);
        int totalCase = 0;
        for (int i = 0; i < dna.size(); i++) {
            totalCase += analizer.analyze(new DnaSample(dna, i, 0), 0, dna.size());
        }
        return totalCase;
    }

    /**
     * Recorre la matriz de arriba hacia abajo
     * @param direction  Posicion incial del recorrdo de la matriz
     * @param dna matriz de adn
     * @return
     */
    private int downSearch(Direction direction, List<String> dna) {
        DnaAnalyzer analizer = factory.get(direction);
        int totalCase = 0;
        for (int i = 0; i < dna.size(); i++) {
            totalCase += analizer.analyze(new DnaSample(dna, 0, i), 0, dna.size());
        }
        return totalCase;
    }

    /**
     * Recorre la matriz de derecha a izquierda
     * @param direction  Posicion incial del recorrdo de la matriz
     * @param dna matriz de adn
     * @return
     */
    private int leftSearch(Direction direction, List<String> dna) {
        DnaAnalyzer analizer = factory.get(direction);
        int totalCase = 0;
        for (int i = dna.size() - 1; i > 0; i--) {
            totalCase += analizer.analyze(new DnaSample(dna, 0, i), i, dna.size());
        }
        return totalCase;
    }

}

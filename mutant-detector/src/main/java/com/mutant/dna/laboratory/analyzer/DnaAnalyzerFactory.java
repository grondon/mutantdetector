package com.mutant.dna.laboratory.analyzer;


import com.mutant.dna.laboratory.dna.DnaComparator;
import com.mutant.dna.laboratory.direction.Direction;
import com.mutant.dna.laboratory.direction.DirectionValidator;
import com.mutant.dna.laboratory.provider.DnaProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class DnaAnalyzerFactory implements DnaProvider<DnaAnalyzer> {


    private Map<Direction, DnaAnalyzer> provider = new HashMap<>();

    DnaProvider<DnaComparator> dnaProvider;
    DnaProvider<DirectionValidator> validatorProvider;

    @Autowired
    public DnaAnalyzerFactory(DnaProvider<DnaComparator> dnaProvider, DnaProvider<DirectionValidator> validatorProvider) {
    this.dnaProvider = dnaProvider;
    this.validatorProvider = validatorProvider;
        provider.put(Direction.DOWN, new DnaAnalyzerDown(getAdnComparator(Direction.DOWN),getDirectionValidator(Direction.DOWN)));
        provider.put(Direction.DOWN_LEFT, new DnaAnalyzerDownLeft(getAdnComparator(Direction.DOWN_LEFT),getDirectionValidator(Direction.DOWN_LEFT)));
        provider.put(Direction.DOWN_RIGTH, new DnaAnalyzerDownRight(getAdnComparator(Direction.DOWN_RIGTH),getDirectionValidator(Direction.DOWN_RIGTH)));
        provider.put(Direction.RIGHT, new DnaAnalyzerRight(getAdnComparator(Direction.RIGHT),getDirectionValidator(Direction.RIGHT)));
    }

    @Override
    public DnaAnalyzer get(Direction direction) {
       return provider.get(direction);
    }

    private DnaComparator getAdnComparator(Direction direction){
       return dnaProvider.get(direction);
    }

    private DirectionValidator getDirectionValidator(Direction direction){
       return validatorProvider.get(direction);
    }
}

package com.mutant.dna.laboratory.dto;

public class MutantDto {


    public String id;
    public boolean mutant;

    public MutantDto() {
    }

    public MutantDto(String id, boolean mutant) {
        this.id = id;
        this.mutant = mutant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isMutant() {
        return mutant;
    }

    public void setMutant(boolean mutant) {
        this.mutant = mutant;
    }
}

package com.mutant.dna.laboratory.config;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@EnableReactiveMongoRepositories
public class AppConfig extends AbstractReactiveMongoConfiguration {

    public MongoClient reactiveMongoClient() {
       return MongoClients.create();
    }

    @Override
    protected String getDatabaseName() {
        return "mutant";
    }


}
package com.mutant.dna.laboratory.repository;

import com.mutant.dna.laboratory.model.People;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface MutantRepository extends ReactiveMongoRepository<People,String> {
}

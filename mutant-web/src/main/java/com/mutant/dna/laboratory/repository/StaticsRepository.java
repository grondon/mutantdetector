package com.mutant.dna.laboratory.repository;

import com.mutant.dna.laboratory.model.Statistic;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface StaticsRepository extends ReactiveMongoRepository<Statistic,String> {

}

package com.mutant.dna.laboratory.service;
import com.mutant.dna.laboratory.exception.MutantException;
import com.mutant.dna.laboratory.model.People;
import reactor.core.publisher.Mono;

public interface LaboratoryService {

    Mono<People>  analyze(People people) throws MutantException;

    Mono<People> save(People people);

    Mono<People> findPeople(People people);
}

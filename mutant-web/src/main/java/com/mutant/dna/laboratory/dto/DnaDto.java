package com.mutant.dna.laboratory.dto;

import java.util.List;

public class DnaDto {

    List<String> dna;

    public DnaDto() {
    }

    public DnaDto(List<String> dna) {
        this.dna = dna;
    }


    public List<String> getDna() {
        return dna;
    }

    public void setDna(List<String> dna) {
        this.dna = dna;
    }

    @Override
    public String toString() {
        return "DnaDto{" +
                "dna=" + dna.size() +
                '}';
    }

}

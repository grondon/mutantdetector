package com.mutant.dna.laboratory.service;

import com.mutant.dna.laboratory.model.Statistic;
import reactor.core.publisher.Mono;

public interface StatisticService {

    Mono<Statistic> incMutant();

    Mono<Statistic> incHuman();

    Mono<Statistic> updateStatistic(boolean isMutant);

    Mono<Statistic> getStatistic();
}

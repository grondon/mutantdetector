package com.mutant.dna.laboratory.service.impl;

import com.mutant.dna.laboratory.model.Statistic;
import com.mutant.dna.laboratory.repository.StaticsRepository;
import com.mutant.dna.laboratory.service.MonoCacheDna;
import com.mutant.dna.laboratory.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class StatisticImpl  implements StatisticService {

    @Autowired
    ReactiveMongoTemplate template;

    @Autowired
    MonoCacheDna cacheDna;

    @Autowired
    StaticsRepository staticsRepository;

    private  String statistic = "32A5014F00A8A895A2C78F5647C4B425";

    @Override
    public Mono<Statistic> incMutant() {
        return updateStatistic("mutant");
    }

    @Override
    public Mono<Statistic> incHuman() {
             return updateStatistic("human");
    }

    private Mono<Statistic> updateStatistic(String filed){
        return template.findAndModify(
                query(where("_id").is(statistic)),
                new Update().inc(filed,1),
                options().returnNew(true).upsert(true),
                Statistic.class
        );
    }

    public Mono<Statistic> updateStatistic(boolean isMutant) {
        if (isMutant)
            return incMutant();
        return incHuman();
    }

    public Mono<Statistic> getStatistic(){
      return cacheDna.getFromChace(Statistic.with(statistic),Statistic.class)
               .switchIfEmpty(getFronDatabase());
    }

    private Mono<Statistic> getFronDatabase(){
        return staticsRepository.findById(statistic)
               .switchIfEmpty(Mono.just(Statistic.with(statistic)));
    }


}


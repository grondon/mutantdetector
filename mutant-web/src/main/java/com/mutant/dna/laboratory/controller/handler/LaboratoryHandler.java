package com.mutant.dna.laboratory.controller.handler;

import com.mutant.dna.laboratory.config.Messages;
import com.mutant.dna.laboratory.dto.DnaDto;
import com.mutant.dna.laboratory.exception.MutantException;
import com.mutant.dna.laboratory.mapper.DnaMapper;
import com.mutant.dna.laboratory.service.LaboratoryService;
import com.mutant.dna.laboratory.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;


@Component
public class LaboratoryHandler {

    @Autowired
    LaboratoryService laboratoryService;

    @Autowired
    StatisticService statisticService;

    @Autowired
    DnaMapper dnaMapper;

    @Autowired
    Messages messages;

    public Mono<ServerResponse> adnAnalyzer(ServerRequest request) {

        return request.bodyToMono(DnaDto.class)
                .map(dnaMapper::mapDnaToPeople)
                .flatMap(this.laboratoryService::analyze)
                .flatMap(this.laboratoryService::save)
                .map(dnaMapper::mapPeopleToDna)
                .filter(mutantDto -> mutantDto.mutant)
                .flatMap(dna -> ServerResponse.ok().body(BodyInserters.fromObject(dna)))
                .switchIfEmpty(ServerResponse.status(HttpStatus.FORBIDDEN).body(BodyInserters.fromObject("")))
                .onErrorResume(e -> Mono.error(new MutantException(messages.get("com.dna.mutant.laboratory"))));
    }

    public Mono<ServerResponse> getStatistic(ServerRequest request) {
        return
                statisticService.getStatistic()
                .map(dnaMapper::mapStadistic)
                .flatMap(sta -> ServerResponse.ok().body(BodyInserters.fromObject(sta)))
                .onErrorResume(e -> Mono.error(new MutantException(messages.get("com.dna.mutant.laboratory"))));
    }

}

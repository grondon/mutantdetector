package com.mutant.dna.laboratory.mapper;

import com.mutant.dna.laboratory.dto.DnaDto;
import com.mutant.dna.laboratory.dto.MutantDto;
import com.mutant.dna.laboratory.dto.StatisticDto;
import com.mutant.dna.laboratory.model.People;
import com.mutant.dna.laboratory.model.Statistic;
import org.springframework.stereotype.Component;

@Component
public class DnaMapper {

    public People  mapDnaToPeople(DnaDto dnaDto){
        People people =new People();
        people.setDna(dnaDto.getDna());
        return people;
    }

    public MutantDto mapPeopleToDna(People people){
        return new MutantDto(people.getId(),people.isMutant());
    }

    public StatisticDto mapStadistic(Statistic statistic){
        return new StatisticDto(statistic.getMutant(),statistic.getHuman());
    }

}

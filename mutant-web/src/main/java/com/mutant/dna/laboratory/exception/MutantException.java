package com.mutant.dna.laboratory.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MutantException extends RuntimeException{

    public MutantException(String message) {
        super(message);
    }
}

package com.mutant.dna.laboratory.service.impl;

import com.mutant.dna.laboratory.analyzer.DnaAnalyzer;
import com.mutant.dna.laboratory.config.Messages;
import com.mutant.dna.laboratory.dna.LaboratoryPkGenerator;
import com.mutant.dna.laboratory.exception.DnaException;
import com.mutant.dna.laboratory.exception.MutantException;
import com.mutant.dna.laboratory.model.People;
import com.mutant.dna.laboratory.model.Statistic;
import com.mutant.dna.laboratory.mutant.Laboratory;
import com.mutant.dna.laboratory.repository.MutantRepository;
import com.mutant.dna.laboratory.service.LaboratoryService;
import com.mutant.dna.laboratory.service.MonoCacheDna;
import com.mutant.dna.laboratory.service.StatisticService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
@Service
public class LaboratoryImpl implements LaboratoryService {


    Logger logger = LogManager.getLogger(DnaAnalyzer.class);

    @Autowired
    ReactiveMongoTemplate template;

    @Autowired
    Laboratory laboratory;

    @Autowired
    LaboratoryPkGenerator laboratoryPkGenerator;

    @Autowired
    MutantRepository mutantRepository;

    @Autowired
    StatisticService statisticService;

    @Autowired
    MonoCacheDna cacheDna;

    @Autowired
    Messages messages;


    @Override
    public Mono<People> analyze(People people) throws MutantException {

        Logger logger = LogManager.getLogger(DnaAnalyzer.class);

        try{
            people.setMutant(laboratory.analysedAdn(people.getDna()));
            people.setId(laboratoryPkGenerator.generate(people.getDna()));
            return Mono.just(people);
        }catch (DnaException ex){
            logger.error(ex.getMessage());
            throw new MutantException(ex.getMessage());
        }

    }

    //  @Cacheable("mutants")
    public Mono<People> save(People people) {
        try {

           return findPeople(people)
                    .switchIfEmpty(
                     mutantRepository
                    .findById(people.getId())
                     .flatMap(this::updateCache)
                    .switchIfEmpty(processPeople(people)));

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return Mono.empty();
        }
    }

    @Override
    public Mono<People> findPeople(People people) {
        return cacheDna.getFromChace(people, People.class);
    }

    private Mono<People> updateCache(People people){
        cacheDna.putInCache(people);
        return Mono.just(people);
    }

    private Mono<People> processPeople(People people){
       return mutantRepository.save(people)
                .flatMap(p->statisticService.updateStatistic(p.isMutant()))
                .flatMap(p->this.updateCache(people))
        ;
    }
}

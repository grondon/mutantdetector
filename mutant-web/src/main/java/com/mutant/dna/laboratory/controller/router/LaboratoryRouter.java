package com.mutant.dna.laboratory.controller.router;

import com.mutant.dna.laboratory.controller.handler.LaboratoryHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;


@Configuration
public class LaboratoryRouter {

    @Bean
    public RouterFunction<ServerResponse> routerMutant(LaboratoryHandler handler){
        return route(
                POST("/mutant").and(accept(MediaType.APPLICATION_JSON)), handler::adnAnalyzer);
    }

    @Bean
    public RouterFunction<ServerResponse> routerFunction(LaboratoryHandler handler){
        return route(
                POST("/stats").and(accept(MediaType.APPLICATION_JSON)), handler::getStatistic);
    }

    @Bean
    public RouterFunction<ServerResponse> getrouterFunction(LaboratoryHandler handler){
        return route(
                GET("/stats").and(accept(MediaType.APPLICATION_JSON)), handler::getStatistic);
    }


}

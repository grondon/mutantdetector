package com.mutant.dna.laboratory.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "statistic")
public class Statistic {


    @Id
    private String id;
    private int mutant;
    private int human;



    public static Statistic with(String id) {
            Statistic st = new Statistic();
            st.setId(id);
            return st;
     }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMutant() {
        return mutant;
    }

    public void setMutant(int mutant) {
        this.mutant = mutant;
    }

    public int getHuman() {
        return human;
    }

    public void setHuman(int human) {
        this.human = human;
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "id='" + id + '\'' +
                ", mutant=" + mutant +
                ", human=" + human +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Statistic)) return false;
        Statistic statistic = (Statistic) o;
        return Objects.equals(getId(), statistic.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId());
    }
}

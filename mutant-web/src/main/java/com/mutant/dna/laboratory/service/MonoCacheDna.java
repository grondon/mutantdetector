package com.mutant.dna.laboratory.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class MonoCacheDna {

    @Autowired
    CacheManager cacheManager;

    public <T> Mono<T> getFromChace(T t, Class<T> classObj) {
        T obj = getCache().get(t, classObj);
        if (obj != null) {
            return Mono.just(obj);
        }

        return Mono.empty();
    }

    public <T> Mono<T> putInCache(T t){
        getCache().put(t,t);
        return Mono.just(t);
    }

    private Cache getCache(){
        return cacheManager.getCache("mutants");
    }

}

package com.mutant.dna.laboratory.dto;

import org.springframework.data.annotation.Id;

public class StatisticDto {

  //  {“count_mutant_dna”:40, “count_human_dna”:100: “ratio”:0.4}

    private int countMutantDna;
    private int countHumanDna;

    public StatisticDto() {
    }

    public StatisticDto(int countMutantDna, int countHumanDna) {
        this.countMutantDna = countMutantDna;
        this.countHumanDna = countHumanDna;
    }

    public int getCountMutantDna() {
        return countMutantDna   ;
    }

    public void setCountMutantDna(int countMutantDna) {
        this.countMutantDna = countMutantDna;
    }

    public int getCountHumanDna() {
        return countHumanDna;
    }

    public void setCountHumanDna(int countHumanDna) {
        this.countHumanDna = countHumanDna;
    }

    public double getRatio() {
        return   countMutantDna==0 || countHumanDna==0 ? 0 : getCountMutantDna()/getCountHumanDna();
    }

}
